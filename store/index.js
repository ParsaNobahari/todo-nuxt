//setting a vuex store allows to handle the state or the data inside
//vuex or vue or nuxt application

//it's how you store your data on a global scale instead of storing
//inside a component


//this is where we task array list is going to be created
export const state = () => ({
    tasks: []
})

//mutation here allows us to mutate or change the state created above
export const mutations = {
    //adding new tasks
    add_task(state, task) {
        state.tasks.push({ content: task, done: false });
        console.log("add_task");
    },
    //removing tasks
    remove_task(state, task) {
        state.tasks.splice(state.tasks.indexOf(task), 1);
    },
    //toggling task state
    toggle_task(state, task) {
        task.done = !task.done;
    },
    //editing task
    edit_task(state, task) {
        //state.tasks.splice(state.list.indexOf(todo), 1, text);
        console.log("edit_task");
    }
}